// 888888 JS Object Notation OR JSON 8888888888

/*SYNTAX:
	{
		"propertyA": "valueA",
		"propertyB": "valueB"
	}
*/
/*
	{
		"city": "Quezon City",
		"province": "NCR",
		"country": "Philippines"
	}

// JSON ARRAYS--------------------------------
	"cities": [
		{ "city": "Quezon City","province": "NCR","country": "Philippines"},
		{ "city": "Caloocan City","province": "NCR","country": "Philippines"},
		{ "city": "Marikina City","province": "NCR","country": "Philippines"}
	]
*/


// JSON METHODS--------------------------------
// "STRINGIFY" method -convert JS objects to strings
let batchesArr=[{batchName: 'Batch x'}, {batchName: 'Batch y'}]

// let's convert the above array to JSON stringify
console.log(">>>>STRINGIFY METHOD result no 1:")
console.log(JSON.stringify(batchesArr));

// SAMPLE no 2:
/*let data={
	name: 'John',
	age : '31',
	address:{
		city: 'Manila',
		country:'Philipines'
	}
}*/

console.log(">>>>STRINGIFY METHOD no 2:")
let data= JSON.stringify({
	name: 'John',
	age : '31',
	address:{
		city: 'Manila',
		country:'Philipines'
	}
})
console.log(data);


console.log(">>Stringify the value/input given by the user:");
// USING STRINGIFY METHOD with variables


// let's make user details, let's use prompt bcos  we don't have css proprties

let firstName= prompt ('What is your first name?');
let lastname= prompt('What is your last name?');
let age = prompt ('What is your age?');
let address= {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city address belong to?')

};
// note: the 1st firstName is the property os otherData, the 2nd firstname is the variable above
// let's store the data from the variable above and convert to strings
let otherData=JSON.stringify({
	firstName:firstName,
	lastname:lastname,
	age: age,
	address: address
})
console.log(otherData);


// CONVERT stringified JSON TO JS OBJECT=======================================================
console.log(">>>>stringified JSON to JS OBJECT")

let batchesJSON= `[{"batchName": "Batch X"},{"batchName": "Batch Y"}]`;

console.log("Result from JSON.parse(variable)");
console.log(JSON.parse(batchesJSON));


console.log("Result from let stringifiedObject=JSON.parse");
let stringifiedObject=JSON.parse(`{"name": "John", "age":"30", "address" : {"city" : "Manila", "country" : "Philippines"}}`)

console.log(stringifiedObject)